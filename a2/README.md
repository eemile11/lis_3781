> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management 

## Elie Emile 

### Assignment 2 # Requirements: 

*Sub-Heading:*

1. Reverse engineer tables. 
2. Populate tables, salt and hash sensitive data.
3. Show screenshot of SQL statement and query results.
4. Grant users 3 & 4 limited access. 

#### README.md file should include the following items:

* git commands
* Screenshots of SQL statements 
* Screenshots of SQL query results


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a git repository 
2. git status- lists the files changed and still need to be worked on
3. git add . - add one or more files
4. git commit- makes changes to files.
5. git push- sends changes to the master branch   
6. git remote add origin https://eemile11@bitbucket.org/eemile11/lis_3781.git- makes chnages to the lis3781 repository
7. git grep- searches the working directory for specific word. 

#### Assignment Screenshots:

* Screenshots od Code and Database

*Screenshot of company table SQL statement:

![picture](images/a2_1.PNG)

*Screenshot of customer table SQL statements*:

![picture](images/a2_2.PNG)

*Screenshot of Company tables results*:

![pictures](images/result1.PNG)

*Screenshot of Customer table results*:

![picture](images/result2.PNG)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*

[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/eemile11/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
