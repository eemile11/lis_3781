> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Elie Emile 

### Assignment # Requirements:
- Populated tables wiith all attributes
- Five records for each table
- ERD must forward engineer

*Sub-Heading:*


#### README.md file should include the following items:

* Assignment requirements
* Git commands 
* Screenshots of ERD 


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a git repository
2. git add . - add one or more files
3. git commit -m - makes changes to files.
4. git push -u origin master - sends changes to the master branch

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](images/ampps_install.PNG)

*Screenshot of ERD*:

![JDK Installation Screenshot](images/a1_erd.PNG)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
