> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Elie Emile 

### Assignment # Requirements:

*Sub-Heading:*

1. Create the ERD.
2. Insert the data into the tables.
3. Make sure the database forward engineers to the server.

#### README.md file should include the following items:

* P1 sql solutions file
* P1 database file
* Screenshot of the completed ERD


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a git repository 
2. git status- lists the files changed and still need to be worked on
3. git add . - add one or more files
4. git commit- makes changes to files.
5. git push- sends changes to the master branch1 

#### Assignment Screenshots:

*Screenshot of the completed database model*:

![Picture](images/p1_erd.png) 




#### Repo Link:

*Repo link*
[lis3781 repo link](https://bitbucket.org/eemile11/lis_3781/src/master/ "lis3781")


