> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management 

## Elie Emile 

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Necessary SQL statements
    - ERD showing all tables and relations 
    - Screenshots of ERD 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Reverse engineered SQL statememts
    - Use salt and hash for sensitive information
    - Screenshot of populated tables 
    - Screenshot of query results 
    
    


3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Creat three tables in the oracle database
    - Populate all tables with at least 5 records
    - Screenshots of SQL solutions
    - Screenshots of table inserts


4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Develop a database system in order ot track day-today business operations of a office supply compnay.
    - Salt and hash social security numbers.
    - Populate all tables in MS SQL server.


5. [A5 README.md](a5/README.md "My A5 README.md file")
    - An extension of A4 
    - Extend the data model’s functionality
    - Develop a smaller data mart as a test platform


6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Design a database in order to track and document the city’scourt case data
    - Salt and hash social security values 
    - Forward engineer the database
    


7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install Mongodb into AMPPS
    - Use a series of commands to make changes in the database
    - Perform inserts, update, deletes
