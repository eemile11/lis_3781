> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Elie Emile 

### Assignment # Requirements:

*Sub-Heading:*

1. Connect to Oracle DB.
2. Create 3 tables: customer, commodity and "order".
3. Populate all tables with at least 5 records each.
4. Screenshots of SQL solutions.
5. Screenshots of populated tables. 

#### README.md file should include the following items:

* git commands
* Screenshots of SQL statements 
* Screenshots of SQL query results



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a git repository 
2. git status- lists the files changed and still need to be worked on
3. git add . - add one or more files
4. git commit- makes changes to files.
5. git push- sends changes to the master branch 

#### Assignment Screenshots:

*Customer table:

![SQL for customer table](images/a3_1.PNG)

*Commodity & "order" Table*:

![SQL for commodity and "order" table](images/a3_2.PNG)

*Inserts for customer, commodity, "order"*:

![Inserts](images/a3_3.PNG)

*Results for customer*:

![Result 1](images/customer.PNG)

*Results for commodity*:

![Result 2](images/commodity.PNG)

*Results for "order"*:

![Result 3](images/order.PNG)


#### Repo Link:

*Repo link*
[lis3781 repo link](https://bitbucket.org/eemile11/lis_3781/src/master/ "lis3781")


